# Chat App

---

### Development

```sh
$ git clone {git_repo_url}
$ cd {project_folder}
$ docker-compose -f docker-compose.dev.yml up -d
```

Then navigate to http://localhost

### Production

```sh
$ git clone {git_repo_url}
$ cd {project_folder}
$ docker-compose up -d
```
