import { api } from './config';

export const getMessagesList = async () => {
  try {
    const { data } = await api.get('/messages');
    return data;
  } catch (error) {
    if (error.response.status === 401) {
      throw new Error('Unauthorized');
      return;
    }
    throw error;
  }
};

export const saveMessage = async payload => {
  try {
    const { data } = await api.post('/messages', payload);
    return data;
  } catch (error) {
    if (error.response.status === 401) {
      throw new Error('Unauthorized');
      return;
    }
    throw error;
  }
};
