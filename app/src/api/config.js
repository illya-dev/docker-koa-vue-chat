import axios from 'axios';
import { apiUrl } from '../config';

const authInterceptor = config => {
  const token = localStorage.getItem('token');

  if (token) {
    config.headers.Authorization = `Bearer ${token}`;
  }

  return config;
};

export const api = axios.create({
  baseURL: apiUrl,
  timeout: 15000,
  headers: {}
});

api.interceptors.request.use(authInterceptor, error => Promise.reject(error));
