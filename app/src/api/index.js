import * as auth from './auth';
import * as users from './users';
import * as messages from './messages';

export default {
  ...auth,
  ...users,
  ...messages
};
