import { api } from './config';

export const getUsersList = async () => {
  try {
    const { data } = await api.get('/users');
    return data;
  } catch (error) {
    if (error.response.status === 401) {
      throw new Error('Unauthorized');
      return;
    }
    throw error;
  }
};
