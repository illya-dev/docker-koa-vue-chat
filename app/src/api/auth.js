import { api } from './config';

export const login = async payload => {
  try {
    return await api.post('/login', payload);
  } catch (error) {
    if (error.response.status === 400) {
      throw new Error('The email address or password is incorrect');
      return;
    }
    throw error;
  }
};

export const register = async payload => {
  try {
    return await api.post('/register', payload);
  } catch (error) {
    if (error.response.status === 400) {
      switch (error.response.data.type) {
        case 'duplicate_key':
          throw new Error('The user with this email already exist');
          break;
      }

      return;
    }

    throw error;
  }
};

export const verifyToken = async () => {
  try {
    return await api.get('/token/verify');
  } catch (error) {
    console.log(error.response.status);
    if (error.response.status === 401) {
      throw new Error('Access token is invalid or expired. Please sign in.');
      return;
    }

    throw error;
  }
};
