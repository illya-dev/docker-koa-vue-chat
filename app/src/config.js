export const isDevelopment = process && process.env.NODE_ENV === 'development';

const serverUrl = window.location.origin;
const apiVersion = 'v1';
const socketVersion = 'v1';

export const apiUrl = `${serverUrl}/api/${apiVersion}`;
export const socketUrl = `${serverUrl}/${socketVersion}`;
