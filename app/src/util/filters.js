export const timeAgo = time => {
  const between = Date.now() / 1000 - Number(time);
  if (between < 3600) {
    return pluralize(Math.floor(between / 60), ' minute');
  } else if (between < 86400) {
    return pluralize(Math.floor(between / 3600), ' hour');
  } else {
    return pluralize(Math.floor(between / 86400), ' day');
  }
};

const dateOptions = {
  year: 'numeric',
  month: 'short',
  day: 'numeric',
  hour: 'numeric',
  minute: 'numeric',
  hour12: false
};

const _intl = new Intl.DateTimeFormat('en-US', dateOptions);

/**
 * Using Intl.DateTimeFormat because of Date.prototype.toLocaleDateString is too slow
 */
export const formatDate = time => {
  const date = new Date(time);

  return _intl.format(date);
};
