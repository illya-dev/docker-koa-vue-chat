export default {
  data: _ => ({
    transitionName: 'fade'
  }),

  mounted() {
    this.applyRouterAnimation();
  },

  methods: {
    applyRouterAnimation() {
      this.$router.beforeEach((to, from, next) => {
        const toDepth = to.path.split('/').length;
        const fromDepth = from.path.split('/').length;
        const transitionName =
          toDepth < fromDepth ? 'slide-right' : 'slide-left';

        this.transitionName = transitionName || DEFAULT_TRANSITION;

        next();
      });
    }
  }
};
