import api from '../../api';
import * as types from '../mutation-types';
import { getMessagesList } from '../../api';
import Vue from 'vue';

const state = {
  messages: [],
  loading: false
};

const getters = {
  getMessages: state => state.messages,
  isMessagesLoading: state => state.loading,
  getMessageById: state => messageId => {
    const messages = state.messages.filter(m => m.id === messageId);
    return messages ? messages[0] : {};
  }
};

const actions = {
  async fetchMessages({ commit }) {
    commit(types.MESSAGES_SET_STATUS_LOADING, true);

    try {
      const messages = await api.getMessagesList();
      commit(types.MESSAGES_UPDATE, messages);
      return Promise.resolve();
    } catch (error) {
      return Promise.reject(error);
    }

    commit(types.MESSAGES_SET_STATUS_LOADING, false);
  },

  async saveMessage({ commit }, payload) {
    Vue.prototype.$socket.emit('send_message', payload);
    return Promise.resolve();
  },

  socket_onMessage({ commit }, message) {
    commit(types.MESSAGES_APPEND_NEW, message);
  }
};

const mutations = {
  [types.MESSAGES_UPDATE](state, messages) {
    state.messages = messages;
  },
  [types.MESSAGES_SET_STATUS_LOADING](state, bool) {
    state.loading = bool;
  },
  [types.MESSAGES_APPEND_NEW](state, message) {
    state.messages.push(message);
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
