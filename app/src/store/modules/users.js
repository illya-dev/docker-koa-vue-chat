import api from '../../api';
import * as types from '../mutation-types';
import { getUsersList } from '../../api';

const state = {
  users: [],
  loading: false
};

const getters = {
  getUsers: state => state.users,
  isUsersLoading: state => state.loading,
  getUserById: state => userId => {
    const users = state.users.filter(u => u.id === userId);
    return users ? users[0] : {};
  }
};

const actions = {
  async fetchUsers({ commit }) {
    commit(types.USERS_SET_STATUS_LOADING, true);

    try {
      const users = await api.getUsersList();
      commit(types.USERS_UPDATE, users);
    } catch (error) {
      return Promise.reject(error);
    }

    commit(types.USERS_SET_STATUS_LOADING, false);
  }
};

const mutations = {
  [types.USERS_UPDATE](state, users) {
    state.users = users;
  },
  [types.USERS_SET_STATUS_LOADING](state, bool) {
    state.loading = bool;
  },
  [types.USERS_PREPEND_NEW](state, user) {
    state.users.unshift(user);
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
