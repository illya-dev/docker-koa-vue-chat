import api from '../../api';
import * as types from '../mutation-types';
import router from '../../router';
import Vue from 'vue';

const state = {
  user: {},
  token: '',
  userLoggedIn: false,
  socketOpened: false
};

const getters = {
  getCurrentUser: state => state.user,
  isUserLoggedIn: state => state.userLoggedIn,
  getUserToken: state => state.token
};

const actions = {
  login: async ({ commit }, payload) => {
    try {
      const { data } = await api.login(payload);

      if (!data.token) return false;

      localStorage.setItem('token', data.token);

      commit(types.SET_USER_LOGGED_IN, true);
      commit(types.SET_USER_TOKEN, data.token);
      commit(types.SET_USER_DATA, data.user);

      router.push({ path: '/' });
    } catch (error) {
      // TODO: Show msg on error
      localStorage.removeItem('token');

      commit(types.SET_USER_LOGGED_IN, false);
      commit(types.SET_USER_DATA, {});
      commit(types.SET_USER_TOKEN, '');

      return Promise.reject(error);
    }
  },

  logout: ({ commit }) => {
    localStorage.removeItem('token');

    commit(types.SET_USER_LOGGED_IN, false);
    commit(types.SET_USER_DATA, {});
    commit(types.SET_USER_TOKEN, '');

    return Promise.resolve();
  },

  register: async ({ commit }, payload) => {
    try {
      const { data } = await api.register(payload);

      return Promise.resolve(data);
    } catch (error) {
      return Promise.reject(error);
    }
  },

  verifyAccess: async ({ commit }) => {
    try {
      const { data } = await api.verifyToken();
      const token = localStorage.getItem('token');

      commit(types.SET_USER_LOGGED_IN, true);
      commit(types.SET_USER_DATA, data);
      commit(types.SET_USER_TOKEN, token);

      return Promise.resolve(data);
    } catch (error) {
      localStorage.removeItem('token');

      commit(types.SET_USER_DATA, {});
      commit(types.SET_USER_LOGGED_IN, false);
      commit(types.SET_USER_TOKEN, '');

      return Promise.reject(error);
    }
  },

  socketOpen: (_, accessToken) => {
    Vue.prototype.$socket.open();
    Vue.prototype.$socket.emit('authenticate', { token: accessToken });
  },

  socketClose: ({ commit }) => {
    Vue.prototype.$socket.close();
    commit(types.SET_SOCKET_STATUS, false);
  },

  socket_authenticated: ({ commit }) => {
    // TODO: Move the room name to a config file
    Vue.prototype.$socket.emit('join', 'chat');
    commit(types.SET_SOCKET_STATUS, true);
  }
};

const mutations = {
  [types.SET_USER_LOGGED_IN](state, bool) {
    state.userLoggedIn = bool;
  },
  [types.SET_USER_DATA](state, user) {
    state.user = user;
  },
  [types.SET_USER_TOKEN](state, token) {
    state.token = token;
  },
  [types.SET_SOCKET_STATUS](state, bool) {
    state.socketOpened = bool;
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
