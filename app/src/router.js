import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

const Login = () => import('./components/pages/Login.vue');
const Register = () => import('./components/pages/Register.vue');
const Logout = () => import('./components/pages/Logout.vue');
const PageNotFoundPage = () =>
  import('./components/pages/PageNotFoundPage.vue');

import Home from './components/pages/Home.vue';

const router = new Router({
  mode: 'history',
  scrollBehavior: () =>
    setTimeout(() => {
      y: 0;
    }, 300),
  routes: [
    {
      path: '/login',
      component: Login,
      meta: {
        title: 'Login'
      }
    },
    {
      path: '/register',
      component: Register,
      meta: {
        title: 'Register'
      }
    },
    {
      path: '/logout',
      component: Logout,
      meta: {
        title: 'Logout'
      }
    },
    {
      path: '/',
      component: Home,
      meta: {
        title: 'Home',
        requiresAuth: true
      }
    },
    {
      path: '*',
      component: PageNotFoundPage,
      meta: {
        title: '404 Not Found'
      }
    }
  ]
});

router.beforeEach((to, from, next) => {
  const isTokenExist = !!localStorage.getItem('token');

  document.title = `Chat | ${to.meta.title}`;

  if (to.matched.some(record => record.meta.requiresAuth) && !isTokenExist) {
    return next({
      path: '/login',
      params: { nextUrl: to.fullPath }
    });
  } else if (
    (to.path === '/login' || to.path === '/register') &&
    isTokenExist
  ) {
    return next({ path: '/' });
  }

  return next();
});

// this.$router.afterEach((to, from, next) => {

//   next();
// });

export default router;
