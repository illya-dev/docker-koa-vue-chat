import Vue from 'vue';
import VeeValidate from 'vee-validate';
import { sync } from 'vuex-router-sync';
import router from './router';
import store from './store';
import * as filters from './util/filters';
import { bus } from './events';

import VueSocketIO from 'vue-socket.io-extended';
import io from 'socket.io-client';
import { socketUrl } from './config';

import App from './App.vue';

import './styles/index.scss';

Vue.config.performance = true;
Vue.prototype.$eventBus = bus;

Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key]);
});

Vue.use(VeeValidate, {
  aria: true,
  classNames: {},
  classes: false,
  delay: 0,
  dictionary: null,
  errorBagName: 'errors', // change if property conflicts
  events: 'input|blur',
  fieldsBagName: 'fields',
  i18n: null, // the vue-i18n plugin instance
  i18nRootKey: 'validations', // the nested key under which the validation messages will be located
  inject: true,
  locale: 'en',
  validity: false
});

const socket = io(socketUrl, {
  autoConnect: false,
  transports: ['websocket']
});

VueSocketIO.defaults = {
  actionPrefix: 'socket'
};

Vue.use(VueSocketIO, socket, { store });

sync(store, router);

new Vue({
  render: h => h(App),
  el: '#app',
  router,
  store,
  sockets: {
    ON_ERROR(message) {
      this.$eventBus.$emit('notify', {
        type: 'error',
        message: message,
        delay: 3000
      });
    }
  }
});
