FROM node:10-alpine

RUN mkdir /usr/app \
    && apk add --no-cache git
WORKDIR /usr/app
COPY package.json yarn.lock ./
RUN yarn
EXPOSE 80
ENTRYPOINT yarn dev