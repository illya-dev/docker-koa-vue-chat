const Message = require('./models/message');

class SocketRouter {
  constructor(io, socket) {
    this.io = io.of('/v1');
    this.socket = socket;
    this.client = {
      firstName: this.socket.decoded_token.firstName,
      lastName: this.socket.decoded_token.lastName
    };

    this._init();
  }

  _init() {
    this._clientConnected();

    this.socket.on('send_message', this._onMessageReceived.bind(this));
    this.socket.on('join', this._joinRoom.bind(this));
    this.socket.on('disconnect', this._clientDisconnected.bind(this));
  }

  socketLogger(msg) {
    console.log('\x1b[36m%s\x1b[0m', `  [WS] ${msg}`);
  }

  async _onMessageReceived(data) {
    try {
      const message = new Message(data);
      const saved = await message.save();
      const response = await Message.findById(saved._id)
        .populate('user', 'firstName lastName email avatar')
        .sort({ createdAt: -1 });

      this._emitMessage(response.toJSON());
    } catch (error) {
      this._emitError(error);
    }
  }

  _clientConnected() {
    this.socketLogger(`Client ${this.getClientFullName(this.client)} connected`);
  }

  _clientDisconnected() {
    this.socketLogger(`Client ${this.getClientFullName(this.client)} disconnected`);
  }

  _emitMessage(message) {
    this.io.in('chat').emit('ON_MESSAGE', message);
  }

  _emitError(e) {
    const msg = e.originalError ? e.originalError.message : e.message;
    this.socket.emit('ON_ERROR', msg);
  }

  _joinRoom(room) {
    if (!room) return;
    this.socket.join(room);
    this.socketLogger(`Client ${this.getClientFullName(this.client)} has joined to the room "${room}"`);
  }

  getClientFullName(client) {
    if (client && client.firstName && client.lastName) {
      return `${client.firstName} ${client.lastName}`;
    }

    return '';
  }
}

module.exports = SocketRouter;
