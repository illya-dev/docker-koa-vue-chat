const Message = require('../models/message');

const getList = async ctx => {
  try {
    let messages = await Message.find({}).populate(
      'user',
      'firstName lastName email avatar'
    );

    ctx.body = messages.map(message => {
      return message.toJSON();
    });
  } catch (error) {
    console.log(error);
    ctx.status = 400;
    ctx.body = error;
  }
};

const add = async ctx => {
  try {
    const message = new Message(ctx.request.body);
    const saved = await message.save();
    const response = await Message.findById(saved._id)
      .populate('user', 'firstName lastName email avatar')
      .sort({ createdAt: -1 });
    ctx.body = response.toJSON();
  } catch (error) {
    ctx.status = 400;
    ctx.body = error;
  }
};

const getById = async ctx => {
  try {
    const message = await Message.findById(ctx.params.id);

    ctx.body = message.toJSON();
  } catch (error) {
    ctx.status = 400;
    ctx.body = error;
  }
};

const update = async ctx => {
  try {
    const id = ctx.params.id;

    const message = await Project.findByIdAndUpdate(id, ctx.request.body, {
      new: true,
      runValidators: true
    });
    if (!message) ctx.status = 204;

    ctx.body = message;
  } catch (error) {
    ctx.status = 400;
    ctx.body = error;
  }
};

// Through WebSockets

const addSockets = async (ctx, data) => {
  try {
    const message = new Message(data);
    const saved = await message.save();
    const response = await Message.findById(saved._id)
      .populate('user', 'firstName lastName email avatar')
      .sort({ createdAt: -1 });
    ctx.socket.broadcast('MESSAGE', response.toJSON());
  } catch (error) {
    ctx.status = 400;
    ctx.body = error;
  }
};

module.exports = {
  getList,
  getById,
  add,
  update,
  addSockets
};
