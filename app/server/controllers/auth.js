const jwt = require('jsonwebtoken');
const passport = require('../middleware/passport');
const User = require('../models/user');
const { JWT_SECRET } = require('../config');

const login = async (ctx, next) => {
  await passport.authenticate('local', (err, user) => {
    if (!user) {
      ctx.status = 400;
      return (ctx.body = {
        message: 'Login failed'
      });
    }

    const token = jwt.sign(user.toJSON(), JWT_SECRET);

    ctx.body = { token: token, user: user.toJSON() };
  })(ctx, next);
};

// TODO: move to utils
const getMongoErrors = e => {
  let errorType = '',
    errorMessages = [];

  if (e.code && e.code === 11000) {
    errorType = 'duplicate_key';
    errorMessages.push('Duplicate key error');
  } else if (e.name && e.name === 'CastError') {
    errorType = 'invalid_request';
    errorMessages.push('Invalid request');
  } else if (e.name && e.name === 'ValidationError') {
    errorType = 'validation_failed';
    errorMessages = Object.keys(e.errors).map(field => e.errors[field].message);
  }

  return {
    type: errorType,
    messages: errorMessages
  };
};

const register = async ctx => {
  try {
    const user = await User.create(ctx.request.body);
    ctx.body = user.toJSON();
  } catch (error) {
    ctx.status = 400;
    ctx.body = getMongoErrors(error);
  }
};

const verifyUser = async ctx => {
  ctx.body = ctx.state.user;
};

module.exports = {
  login,
  register,
  verifyUser
};
