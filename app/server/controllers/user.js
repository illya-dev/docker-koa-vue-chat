const User = require('../models/user');

const getList = async ctx => {
  try {
    let users = await User.find({});

    ctx.body = users.map(user => {
      return user.toJSON();
    });
  } catch (error) {
    ctx.status = 400;
    ctx.body = error;
  }
};

const getById = async ctx => {
  try {
    const user = await User.findById(ctx.params.id);

    ctx.body = user.toJSON();
  } catch (error) {
    ctx.status = 400;
    ctx.body = error;
  }
};

const getCurrent = async ctx => {
  ctx.body = ctx.req.user;
};

module.exports = {
  getList,
  getById,
  getCurrent
};
