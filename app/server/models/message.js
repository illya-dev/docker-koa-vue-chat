const mongoose = require('mongoose');
// const { MongooseAutoIncrementID } = require('mongoose-auto-increment-reworked');

const messageSchema = new mongoose.Schema(
  {
    content: {
      type: String,
      required: 'Message content is required'
    },

    user: {
      type: mongoose.Schema.Types.ObjectId,
      required: 'User id is required',
      ref: 'User'
    }
  },
  {
    timestamps: true
  }
);

messageSchema.options.toJSON = {
  transform: function(doc, ret, options) {
    ret.id = ret._id;
    delete ret._id;
    delete ret.__v;

    return ret;
  }
};

// const autoIncrementPlugin = new MongooseAutoIncrementID(
//   messageSchema,
//   'Message'
// );
// autoIncrementPlugin.applyPlugin();

const Message = mongoose.model('Message', messageSchema);

module.exports = Message;
