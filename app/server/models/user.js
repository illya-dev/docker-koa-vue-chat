const mongoose = require('mongoose');
const crypto = require('crypto');
// const { MongooseAutoIncrementID } = require('mongoose-auto-increment-reworked');

const userSchema = new mongoose.Schema(
  {
    firstName: {
      type: String,
      required: 'First name is required'
    },
    lastName: {
      type: String,
      required: 'Last name is required'
    },
    email: {
      type: String,
      required: 'Email is required',
      unique: 'The email already exist'
    },
    passwordHash: {
      type: String,
      required: 'Password is required'
    },
    salt: String,
    role: {
      type: [String], // Array of roles
      default: ['developer'],
      enum: [
        'admin',
        'project_manager',
        'quality_assurance',
        'sales_manager',
        'developer'
      ]
    },
    avatar: {
      type: String,
      default: ''
    }
  },
  {
    timestamps: true
  }
);

userSchema
  .virtual('password')
  .set(function(password) {
    this._plainPassword = password;
    if (password) {
      this.salt = crypto.randomBytes(128).toString('base64');
      this.passwordHash = crypto.pbkdf2Sync(
        password,
        this.salt,
        1,
        128,
        'sha1'
      );
    } else {
      this.salt = undefined;
      this.passwordHash = undefined;
    }
  })

  .get(function() {
    return this._plainPassword;
  });

userSchema.virtual('canManageProject').get(function() {
  return (
    this.role.indexOf('admin') >= 0 || this.role.indexOf('project_manager') >= 0
  );
});

userSchema.virtual('canManageUsers').get(function() {
  return (
    this.role.indexOf('admin') >= 0 || this.role.indexOf('project_manager') >= 0
  );
});

userSchema.methods.checkPassword = function(password) {
  if (!password) return false;
  if (!this.passwordHash) return false;
  return (
    crypto.pbkdf2Sync(password, this.salt, 1, 128, 'sha1') == this.passwordHash
  );
};

userSchema.options.toJSON = {
  transform: function(doc, ret, options) {
    ret.id = ret._id;
    delete ret._id;
    delete ret.__v;
    delete ret.passwordHash;
    delete ret.salt;

    return ret;
  }
};

// const autoIncrementPlugin = new MongooseAutoIncrementID(userSchema, 'User');
// autoIncrementPlugin.applyPlugin();

const User = mongoose.model('User', userSchema);

module.exports = User;
