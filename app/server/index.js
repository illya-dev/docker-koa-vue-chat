const Koa = require('koa');
const bodyParser = require('koa-bodyparser');
const cors = require('@koa/cors');
const logger = require('koa-logger');
const passport = require('koa-passport');

const WS = require('socket.io');
const http = require('http');
const router = require('./router');
const SocketsRouter = require('./sockets');
const socketJWT = require('./middleware/socketjwt');

const mongoConnect = require('./db');

const isProd = process.env.NODE_ENV === 'production';
const setup = isProd ? require('./utils/setup-prod.js') : require('./utils/setup-dev.js');

(async () => {
  const app = new Koa();

  await mongoConnect();

  // API
  app
    .use(function(ctx, next) {
      return next().catch(err => {
        if (err.status === 401) {
          ctx.status = 401;
          ctx.body = {
            error: err.originalError ? err.originalError.message : err.message
          };
        } else {
          throw err;
        }
      });
    })
    .use(cors())
    .use(passport.initialize())
    .use(logger())
    .use(bodyParser())
    .use(router.routes())
    .use(router.allowedMethods());

  setup(app);

  // WS
  const server = http.Server(app.callback());
  const io = new WS(server);

  io.of('/v1')
    .on('connection', socketJWT)
    .on('authenticated', socket => {
      let socketsRouter = new SocketsRouter(io, socket);
    });

  server.listen(process.env.PORT || 3000, () => {
    console.log('Server running successfully');
  });
})();
