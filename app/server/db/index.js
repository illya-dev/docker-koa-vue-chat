const mongoose = require('mongoose');
const crypto = require('crypto');

mongoose.Promise = Promise;
// mongoose.set('debug', true);

module.exports = async () => {
  return await mongoose.connect('mongodb://mongo:27017', {
    useNewUrlParser: true,
    user: 'c4wchat',
    pass: 'sOOp3rs3cr3tpa55w0rd',
    dbName: 'chat'
  });
};
