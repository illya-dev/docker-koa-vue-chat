const passport = require('koa-passport');
const LocalStrategy = require('passport-local');
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;

const User = require('../models/user');
const { JWT_SECRET } = require('../config');

const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: JWT_SECRET,
  passReqToCallback: true
};

const localOptions = {
  usernameField: 'email',
  passwordField: 'password',
  session: false
};

const validateUserLocal = async (email, password, done) => {
  try {
    const user = await User.findOne({ email });

    if (!user || !user.checkPassword(password)) {
      return done(null, false, {
        message: 'Email or password are invalid.'
      });
    }

    return done(null, user);
  } catch (error) {
    return done(error);
  }
};

const validateUserJwt = async (req, payload, done) => {
  try {
    const user = await User.findById(payload.id);
    return done(null, user.toJSON() || false);
  } catch (error) {
    return done(error, false);
  }
};

passport.use(new LocalStrategy(localOptions, validateUserLocal));
passport.use(new JwtStrategy(jwtOptions, validateUserJwt));

module.exports = passport;
