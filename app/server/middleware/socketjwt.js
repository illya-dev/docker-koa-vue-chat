const socketioJwt = require('socketio-jwt');
const { JWT_SECRET } = require('../config');

module.exports = socketioJwt.authorize({
  secret: JWT_SECRET,
  timeout: 3000,
  callback: false
  // handshake: true
});
