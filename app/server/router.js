const Router = require('koa-router');
const jwt = require('./middleware/jwt');

const AuthController = require('./controllers/auth');
const UserController = require('./controllers/user');
const MessageController = require('./controllers/message');

const router = new Router();

router.prefix('/api/v1');

// Auth
router.post('/register', AuthController.register);
router.post('/login', AuthController.login);
router.get('/token/verify', jwt, AuthController.verifyUser);

// Users
router.get('/users', jwt, UserController.getList);
router.get('/users/me', jwt, UserController.getCurrent);
router.get('/users/:id', jwt, UserController.getById);

// Messages
router.get('/messages', jwt, MessageController.getList);
router.post('/messages', jwt, MessageController.add);
router.get('/messages/:id', jwt, MessageController.getById);

router.all('*', function(ctx) {
  ctx.status = 404;
  ctx.body = {
    message: 'Route not found'
  };
});

module.exports = router;
