events {
  worker_connections 1024;
}

http {
    include mime.types;
    # Expires map
    map $sent_http_content_type $expires {
        default                    off;
        text/html                  epoch;
        text/css                   max;
        application/javascript     max;
        ~images/                   max;
        ~fonts/                    max;
    }

    server {
        listen 80;

        gzip on;
        # gzip_disable "msie6";
        gzip_types text/plain text/css application/json application/x-javascript text/xml application/xml application/xml+rss text/javascript application/javascript;

        expires $expires;

        location / {
            root    /var/www/static;
            try_files $uri $uri/index.html @node;
        }

        location @node {
            proxy_http_version 1.1;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection "upgrade";
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header Host $http_host;
            proxy_set_header X-NginX-Proxy true;
            proxy_max_temp_file_size 0;
            proxy_redirect off;
            proxy_read_timeout 120s;
            proxy_pass http://app;
        }
    }
}